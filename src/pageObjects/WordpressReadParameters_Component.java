package pageObjects;

import main.BrowserFactory;
import main.Main;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import static extent.reports.ExtentManager.logInfo;
import static extent.reports.ExtentManager.logPass;

public class WordpressReadParameters_Component {

    private static WebDriver driver;

    public WordpressReadParameters_Component(){
        driver = BrowserFactory.getDriver();
        PageFactory.initElements(driver, this);
    }

    public String getPassword(){
        if(Main.parameters.getParameter("positive").equals("true")){
            return "training";
        } else {
            return "abcdefgh";
        }
    }

    public String getProduct(){
        logInfo("Trying to read product name from internal sources.");
        logPass("Product name has been read successfully.");
        return Main.parameters.getParameter("product");
    }

    public String getCouponName(){
        logInfo("Trying to read coupon name from internal sources.");
        logPass("Coupon name has been read successfully.");
        return Main.parameters.getParameter("coupon");
    }

    public void step1() throws InterruptedException {
        logInfo("Trying to read login from internal sources.");
        Thread.sleep(500);
        logPass("Login has been read successfully.");
    }

    public void step2() throws InterruptedException {
        logInfo("Trying to read password from internal sources.");
        Thread.sleep(500);
        logPass("Password has been read successfully.");
    }
}
