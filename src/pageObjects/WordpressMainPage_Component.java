package pageObjects;

import main.BrowserFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static extent.reports.ExtentManager.logInfo;
import static extent.reports.ExtentManager.logPass;
import static steps.waits.WaitMethods.*;

public class WordpressMainPage_Component {

    private static WebDriver driver;

    public WordpressMainPage_Component(){
        driver = BrowserFactory.getDriver();
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "user_login")
    private WebElement loginForm;

    @FindBy(id = "user_pass")
    private WebElement passForm;

    @FindBy(id = "wp-submit")
    private WebElement submitBtn;

    @FindBy(xpath = "//a/span[contains(text(),'Add New Invoice')]")
    private WebElement newInvBtn;

    @FindBy(xpath = "//a/span[contains(text(),'Reports')]")
    private WebElement reportsBtn;

    @FindBy(xpath = "//a/span[contains(text(),'Shop')]")
    private WebElement shopBtn;


    public void logIn(String loginName, String password){
        waitForJQueryLoad();
        waitUntilElementIsVisible(loginForm,10);
        loginForm.clear();
        new Actions(BrowserFactory.getDriver()).click().sendKeys(loginForm, loginName).build().perform();
        logInfo("Login typed into input field.");
        new Actions(BrowserFactory.getDriver()).click().sendKeys(passForm, password).build().perform();
        logInfo("Password typed into input field.");
        submitBtn.click();
        logInfo("Clicked on Login button.");
        waitForJQueryLoad();
    }

    public void checkIfLoggingSuccess() throws InterruptedException {
        if(!waitUntilElementIsNotPresent(submitBtn,10)){
            throw new AssertionError("Process of logging was unsuccessful.");
        } else {
            logPass("Process of logging was successful.");
        }
    }

    public void addNewInvoice(){
        waitUntilElementIsVisible(newInvBtn,15);
        newInvBtn.click();
        logInfo("Clicked on 'Add New Invoice' button.");
    }

    public void goToReports(){
        waitUntilElementIsVisible(reportsBtn,15);
        reportsBtn.click();
        logInfo("Clicked on 'Reports' button.");
    }

    public void goToShop(){
        waitUntilElementIsVisible(shopBtn,15);
        shopBtn.click();
        logInfo("Clicked on 'Shop' button.");
    }
}
